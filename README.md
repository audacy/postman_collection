# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Collection to test the new schedules api implementation on the Audacy gateway 
* Version 0.11
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Etch an SD card with the latest 1.6.0 schedules gateway image
* Power up gateway using the SD card and set up serial number and for dhcp
* Run the collection in order to methodically test schedules implementation

### Contribution guidelines ###

* New test cases can be added as coverage gaps are identified

### Who do I talk to? ###

* This collection is being maintained by Ashik Rahman
* Justin Eltoft can be contacted as well for support
